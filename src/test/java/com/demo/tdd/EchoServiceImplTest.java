package com.demo.tdd;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.demo.tdd.services.EchoService;
import com.demo.tdd.services.EchoServiceImpl;

class EchoServiceImplTest {

	private EchoService echoService = new EchoServiceImpl();

	@Test
	void testEcho() {
		assertEquals("ABC", echoService.echo("ABC"));
	}
}